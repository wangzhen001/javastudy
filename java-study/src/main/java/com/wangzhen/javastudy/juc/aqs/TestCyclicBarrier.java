package com.wangzhen.javastudy.juc.aqs;

import com.wangzhen.javastudy.util.SleepUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CyclicBarrier;

/**
 * Description: 测试 CyclicBarrier
 * Datetime:    2020/12/15   9:55
 * Author:   王震
 * desc: CyclicBarrier 让一组线程达到一个屏障时被阻塞，直到最后一个线程达到屏障时，屏障才会开门 所有被屏障拦截的线程才会继续干活
 *       一般用于，将一个任务分解为多个任务处理完成后，在合并处理的情形
 *
 *       CountDownLatch : 一个线程(或者多个)， 等待另外N个线程完成某个事情之后才能执行。  
 *       CyclicBarrier  : N个线程相互等待，任何一个线程完成之前，所有的线程都必须等待。
 *          这样应该就清楚一点了，对于CountDownLatch来说，重点是那个“一个线程”, 是它在等待， 而另外那N的线程在把“某个事情”做完之后可以继续等待，可以终止。
 *          而对于CyclicBarrier来说，重点是那N个线程，他们之间任何一个没有完成，所有的线程都必须等待。
 *      CountDownLatch 是计数器, 线程完成一个就记一个, 就像 报数一样, 只不过是递减的.
 *      而CyclicBarrier更像一个水闸, 线程执行就想水流, 在水闸处都会堵住, 等到水满(线程到齐)了, 才开始泄流.
 */
@Slf4j
public class TestCyclicBarrier {



    @Test
    public void test1() throws IOException {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(5,()->{
            log.info("{}人都到了，开始开会",5);
        });
        for (int i = 0; i < 5; i++) {
            int finalI = i;
            new Thread(()->{
                try {
                    SleepUtils.second(new Random().nextInt(5));
                    log.info("{}到会了", finalI);
                    cyclicBarrier.await();
                    log.info("收到，开会了");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
        System.in.read();
    }
}