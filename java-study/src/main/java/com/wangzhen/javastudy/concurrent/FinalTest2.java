package com.wangzhen.javastudy.concurrent;

import java.util.ArrayList;

/**
 * @description:
 * @datetime: 2021/3/30   下午7:08
 * @author: 王震
 */
public class FinalTest2 {
    public static void main(String[] args) {
        FinalTest2 finalTest2 = new FinalTest2();
        ArrayList<Integer> list = new ArrayList<>();
        int i =2;
        // 这种写法会产生一个类
        Thread thread = new Thread(){
            @Override
            public void run() {
                int j =i;
                j++;
            }
        };

        // 这种写法不会产生2个类
        Thread thread2 =new Thread(()->{
            int j = i;
            j++;
        });
        thread.start();
    }
}
