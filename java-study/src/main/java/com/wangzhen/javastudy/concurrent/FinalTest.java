package com.wangzhen.javastudy.concurrent;

import org.junit.Test;

/**
 * Description: 测试final 修饰属性相关的代码
 * Datetime:    2021/2/26   下午6:15
 * Author:   王震
 *
 */
public class FinalTest {
    int a =3;

    /**
     *
     * 在Lambda表达式中可以捕获静态变量和实例变量，但是如果想要捕获局部变量的时候就需要声明成final的，
     * 即使我们不主动声明，编译器也会为我们自动声明成final的，不能再重新赋值。也就是Lambda表达式中访
     * 问的局部变量(隐式被声明为final的)是可读不可写的，但是Lambda表达式中访问的实例变量和静态变量是
     * 可读可写的
     * @throws InterruptedException
     */
    @Test
    public void test02() throws InterruptedException {
        int i =2;
        // i=3;
        Thread thread = new Thread(() -> {
            int j = i+1;
            a=4; //实例变量是可以访问的
           // i= 2;
            System.out.println(j);
        });
        thread.start();
        // i=3; 这里会报错，因为在匿名类中final 类型的变量会被隐士的加上final修饰
    }

    @Test
    public void test03(){
        FinalTest finalTest = new FinalTest();
        Thread thread = new Thread(() -> {
            Class clz = finalTest.getClass();
        });
        thread.start();
    }

}
