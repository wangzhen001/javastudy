package com.wangzhen.javastudy.concurrent;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @description: lambda 相关的测试
 * @datetime: 2021/3/30   下午7:15
 * @author: 王震
 */
public class LambdaTest {


    /**
     *  在lamda内部使用变量其默认会被final修饰 不管是值传递还是应用传递都不能被修改。
     *  1.外层局部变量是如何传入lambda的
     *  lambda表达式的本质是什么？也就是匿名内部类，或者再精确一点，就是一个函数式接口的实现的实例嘛
     *  那再想一下，你把外层局部变量拿到这个实例去使用，这个变量是如何传进去的？是通过普通方法传参吗？
     *  当然不是，你参数列表又不能改变，答案是构造器嘛。lambda表达式实例化的时候，编译器会创建一个新
     *  的class文件（想一下你是不是在工程编译之后见到过类似于Main$1.class的文件），该文件就是
     *  lambda实例化的类的字节码文件，在该文件中，编译器帮我们创建了一个构造器，该构造器的入参中就
     *  包含了你要使用的外层局部变量，所以外层局部变量就通过lambda的构造器传入实例内部供其使用
     */
    @Test
    public void test01(){
        int a =1;
        ArrayList<Integer> list = new ArrayList<>();
        Thread thread = new Thread(() -> {
            int b =a+1;
            list.add(10);
        });
        thread.start();
    }
}
