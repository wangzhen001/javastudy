package com.wangzhen.algorithm.Interview.ThreadPrint2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * @description: 创建两个线程，一个线程put 数据，另一个线程检查数据，等检查到5的时候打印并退出
 * @datetime: 2021/4/10   下午5:11
 * @author: 王震
 */
public class ThreadPrint2 {
    static List<Integer> list = new ArrayList<>(10);
    static Semaphore s1 = new Semaphore(0);
    static Semaphore s2 = new Semaphore(0);
    public static  void add(int i){
        try {
            s1.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        list.add(i);
        s2.release();
    }
    public static  int size(){
        try {
            s2.acquire();
            return list.size();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 0;
        }finally {
            s1.release();
        }

    }

    public static void main(String[] args) throws IOException {
        Thread thread = new Thread(() -> {
            s1.release();
            for (int i = 0; i < 10; i++) {
                System.out.println("增加i"+i);
                add(i);
            }
        });

        Thread thread1 = new Thread(() -> {
            while (true){
                if(size()==5){
                    System.out.println("容器数量已经达到5，线程二退出");
                    break;
                }
            }
        });
        thread.start();
        thread1.start();
        System.in.read();
    }
}
